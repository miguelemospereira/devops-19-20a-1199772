package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void setFirstNameSuccessCase() {
        //Arrange:
        Employee employee = new Employee("Marta", "Pinheiro", "student", "developer");
        String expected = "Marta";

        //Act:
        String result = employee.getFirstName();

        //Assert:
        assertEquals(expected, result);
    }

    @Test
    void setLastNameSuccessCase() {
        //Arrange:
        Employee employee = new Employee("Marta", "Pinheiro", "student", "developer");
        String expected = "Pinheiro";

        //Act:
        String result = employee.getLastName();

        //Assert:
        assertEquals(expected, result);
    }

    @Test
    void setDescriptionSuccessCase() {
        //Arrange:
        Employee employee = new Employee("Marta", "Pinheiro", "student", "developer");
        String expected = "student";

        //Act:
        String result = employee.getDescription();

        //Assert:
        assertEquals(expected, result);
    }

    @Test
    void setJobTitleSuccessCase() {
        //Arrange:
        Employee employee = new Employee("Marta", "Pinheiro", "student", "developer");
        String expected = "developer";

        //Act:
        String result = employee.getJobTitle();

        //Assert:
        assertEquals(expected, result);
    }

}